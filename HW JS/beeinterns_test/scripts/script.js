// var toUpperText = document.querySelectorAll('.lecture-name .structure');
// toUpperText.forEach(function(Element) {
//     Element.innerHTML = Element.innerHTML.toUpperCase();
// });

var textLengthLimited = document.querySelectorAll('.gray');

textLengthLimited.forEach(function(Element) {
    if (Element.innerHTML.length > 20) {
        var txt = "";
        for (var i = 0; i < 20; i++)
            txt += Element.innerHTML[i];
        for(var i = 20; i < 23; i++)
            txt += '.';

        Element.innerHTML = txt;
    }
});


const toggle = document.querySelectorAll('.deactive')

toggle[0].addEventListener('click', function (){    
    const ul = document.querySelector('.first_ul')
    
    ul.style.display = ul.style.display === 'block' ? 'none' : 'block'
    this.classList.toggle('active');
    
});
toggle[1].addEventListener('click', function (){
    const ul = document.querySelector('.second_ul')
    
    ul.style.display = ul.style.display === 'block' ? 'none' : 'block'
    this.classList.toggle('active');
});


const lectureList = document.querySelectorAll('.class-li-lecture')


function countLection(Element) {
    document.getElementById("sum").innerHTML = lectureList.length + " лекций"
}
countLection(lectureList)


for(let i=0; i<lectureList.length; i++) {
    var dupLecture = lectureList[i].cloneNode(true)
    if (i>=4){
        dupLecture.style.display = 'none' 
    }
    ul.append(dupLecture);
}

const allLectureList = document.querySelectorAll('.class-li-lecture')
const arrow = document.querySelectorAll('.allArrow')

arrow[1].addEventListener('click', function (){    

    for(let i=0; i<allLectureList.length; i++) {
        if (i<4){
            allLectureList[i].style.display = 'none' 
        } else if(i>=4){
            allLectureList[i].style.display = 'list-item'
        }
        ul.append(dupLecture);
    }
});
arrow[0].addEventListener('click', function (){    

    
    for(let i=0; i<allLectureList.length; i++) {
        if ((i>=4)&&(i<8)){
            allLectureList[i].style.display = 'none' 
        } else if(i<4) {
            allLectureList[i].style.display = 'list-item'
        }
        ul.append(dupLecture);
    }
});



for (let i=0; i<lectureList.length; i++){
    group = lectureList[i].dataset.group;
}


const html = document.getElementById('html')
const css = document.getElementById('css')
const js = document.getElementById('js')



html.addEventListener('click', function (){    

    for(let i=0; i<lectureList.length; i++) {
        group = lectureList[i].dataset.group;
        if (group == "html"){
            allLectureList[i].style.display = 'list-item' 
        } else{
            allLectureList[i].style.display = 'none'
        }
    }
});
css.addEventListener('click', function (){    

    for(let i=0; i<lectureList.length; i++) {
        group = lectureList[i].dataset.group;
        if (group == "css"){
            allLectureList[i].style.display = 'list-item' 
        } else{
            allLectureList[i].style.display = 'none'
        }
    }
});
js.addEventListener('click', function (){    

    for(let i=0; i<lectureList.length; i++) {
        group = lectureList[i].dataset.group;
        if ((group == "javascript") || (group == "react")){
            allLectureList[i].style.display = 'list-item' 
        } else{
            allLectureList[i].style.display = 'none'
        }
    }
});


function groupObject() {
    const lectureObject = {}
    const key =["title", "description","date","image","label"]
    for(let i=0; i<lectureList.length; i++) {
        group = lectureList[i].dataset.group;
        const lectureInfo = {}

        title = lectureList[i].querySelector(".structure").innerHTML
        lectureInfo['title'] = title

        description = lectureList[i].querySelector(".description").innerHTML
        lectureInfo['description'] = description

        date = lectureList[i].querySelector(".start").innerHTML
        lectureInfo['date'] = date

        image = lectureList[i].querySelector(".img").getAttribute('src')
        lectureInfo['image'] = image

        lectureTitle = lectureList[i].querySelector(".lecture-title").innerHTML
        lectureInfo['lebel'] = lectureTitle

        if (!(group in lectureObject)) lectureObject[group] = []

        lectureObject[group].push(lectureInfo)

    }

    console.log(lectureObject);
    return lectureObject
}

groupObject()